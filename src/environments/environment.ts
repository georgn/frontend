// The file contents for the current environment will overwrite these during build.
// The build de.ansolution.stockmanager.system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The groups of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    // url: 'https://api.' + window.location.hostname,
    url: 'http://localhost:8080',
    version: 'DEVELOP'
};
