import {Component, HostListener, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UrlHistoryService} from "./shared/services/url-history.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(private router: Router, private urlHistory: UrlHistoryService) {
    }

    ngOnInit() {
    }
}
