import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {faSignOutAlt} from "@fortawesome/free-solid-svg-icons";

@Component({
    selector: 'app-dashboard-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    faSignOut = faSignOutAlt;

    constructor(private translate: TranslateService, public router: Router) {
        this.translate.setDefaultLang('de');
        this.translate.use( 'de');

    }

    ngOnInit() {
    }

}
