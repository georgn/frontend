import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {HeaderComponent} from "./components/header/header.component";

@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        FontAwesomeModule,
    ],
    declarations: [DashboardComponent, HeaderComponent,]
})
export class DashboardModule {}
