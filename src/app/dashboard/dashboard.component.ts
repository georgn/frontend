import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {faPencilAlt, faUserCircle} from "@fortawesome/free-solid-svg-icons";
import {Router} from "@angular/router";
import {routerTransition} from "../router.animations";
import {Employee} from "../shared/objects/employee";
import {RestService} from "../shared/rest/rest.service";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    faUser = faUserCircle;
    faPencil = faPencilAlt;
    employees: Array<Employee> = [];

    private restService: RestService<Employee>;

    constructor(private httpClient: HttpClient, private router: Router) {
        this.restService = new RestService<Employee>(this.httpClient, 'employee');
    }

    ngOnInit() {
        document.querySelector('body').style.backgroundColor = '#EEF2F6FF';
        this.loadEmployees();
    }

    private loadEmployees() {
        this.restService.getList('').then(data => {
            this.employees = data;
        });
    }

    addEmployee() {
        this.router.navigateByUrl('employee');
    }

    getEmployeeName(employee: Employee) {
        return `${employee.firstname} ${employee.name}`.trim();
    }

    editEmployee(employee: Employee) {
        this.router.navigateByUrl('/employee/' + employee.id);
    }
}
