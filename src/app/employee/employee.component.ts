import {Component, OnInit} from '@angular/core';
import {RestService} from "../shared/rest/rest.service";
import {Employee, Skill} from "../shared/objects/employee";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'app-employee',
    templateUrl: './employee.component.html',
    styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

    title: string = 'Mitarbeiter anlegen';
    employee: Employee = new Employee();
    skills: Array<Skill> = [];

    private restService: RestService<Employee>;

    constructor(private httpClient: HttpClient,
                public router: Router,
                private activatedRoute: ActivatedRoute,
                private toastr: ToastrService
    ) {
        this.restService = new RestService<Employee>(this.httpClient, 'employee');
    }

    ngOnInit() {
        document.querySelector('body').style.backgroundColor = '#EEF2F6FF';
        let id = this.activatedRoute.snapshot.paramMap.get('id');

        if (id) {
            this.restService.get(id)
                .then(data => {
                    this.employee = data;
                    for (let skill of this.employee.skills) {
                        let tmpSkill = this.skills.find(item => item.name === skill.name);
                        if (tmpSkill) {
                            tmpSkill.active = true;
                        }
                    }
                    this.title = `${this.employee.firstname} ${this.employee.name}`.trim();
                })
                .catch(error => {
                    if (error.status === 404) {
                        this.toastr.error('Mitarbeiter konnte nicht gefunden werden!');
                    }
                });
        }

        this.restService.getListGeneric<Skill>('skills').then(data => this.skills = data);
    }

    save() {
        let skills = [];
        for (let skill of this.skills) {
            if (skill.active) {
                skills.push(skill);
            }
        }

        this.employee.skills = skills;

        if (this.employee.id) {
            this.restService.post(this.employee, this.employee.id.toString())
                .then(data => {
                    this.toastr.success('Mitarbeiter erfolgreich gespeichert!');
                })
        } else {
            this.restService.post(this.employee)
                .then(data => {
                    this.toastr.success('Mitarbeiter erfolgreich angelegt!');
                    this.router.navigateByUrl('/');
                })
        }
    }
}
