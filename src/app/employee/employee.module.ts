import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EmployeeComponent} from './employee.component';
import {EmployeeRoutingModule} from "./employee-routing.module";
import {FormsModule} from "@angular/forms";


@NgModule({
    declarations: [EmployeeComponent],
    imports: [
        CommonModule,
        EmployeeRoutingModule,
        FormsModule
    ]
})
export class EmployeeModule {
}
