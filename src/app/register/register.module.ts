import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {RegisterRoutingModule} from "./register-routing.module";
import {RegisterComponent} from "./register.component";

@NgModule({
    imports: [
        CommonModule,
        RegisterRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        TranslateModule
    ],
    declarations: [RegisterComponent]
})
export class RegisterModule {
}
