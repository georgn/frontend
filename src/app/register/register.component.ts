import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {routerTransition} from '../router.animations';
import {AuthenticationService} from '../shared/services/authentication.service';
import {HttpClient} from "@angular/common/http";
import {RestService} from "../shared/rest/rest.service";
import {User} from "../shared/objects/user";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    animations: [routerTransition()]
})
export class RegisterComponent implements OnInit {
    loading = false;
    user = new User();
    password2: string = null;

    constructor(private httpClient: HttpClient, private toastr: ToastrService, private router: Router) {
    }

    ngOnInit() {
    }

    register() {
        let http = new RestService<User>(this.httpClient, 'auth');
        http.post(this.user, 'register').then(data => {
            this.toastr.success('Erfolgreich registriert');
            this.router.navigateByUrl('/login');
        }).catch(error => {
            if (error.status === 400) {
                this.toastr.error('Die Firma oder die E-Mail existiert bereits!');
            }
        });
    }

}
