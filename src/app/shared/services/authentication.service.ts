import {Injectable} from '@angular/core';
import {HttpClient,} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class AuthenticationService {


    constructor(private httpClient: HttpClient) {
    }

    login(username: string, password: string) {
        return new Promise<any>((resolve, reject) => {
            this.httpClient.post<any>(
                `${environment.url}/auth/login`,
                {mail: username, password: password},
                {observe: 'response'}
            ).subscribe(
                resp => {
                    const token = resp.headers.get('Authorization');
                    console.log(token)
                    if (token) {
                        localStorage.setItem('token', token);
                    }
                    resolve(token);
                },
                error => {
                    reject(error);
                }
            );
        });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('token');
        localStorage.removeItem('mail');
        localStorage.removeItem('user');
    }
}
