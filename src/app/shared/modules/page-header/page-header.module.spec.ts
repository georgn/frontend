import { PageHeaderModule } from './page-header.module';

describe('DatepickerI18nModule', () => {
  let pageHeaderModule: PageHeaderModule;

  beforeEach(() => {
    pageHeaderModule = new PageHeaderModule();
  });

  it('should create an instance', () => {
    expect(pageHeaderModule).toBeTruthy();
  });
});
