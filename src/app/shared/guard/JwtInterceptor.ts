import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {ToastrService} from "ngx-toastr";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private toastr: ToastrService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.url.startsWith(environment.url)) {
            // add authorization header with jwt token if available
            const token = localStorage.getItem('token');
            if (token) {
                request = request.clone({
                    setHeaders: {
                        Authorization: token
                    }
                });
            }
        }

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                return event;
            }), catchError((error: HttpErrorResponse) => {
                console.log(error);
                if (error.status === 401) { // catch unauthorized error
                    // redirect to login
                    const port = window.location.port !== '80' ? ':' + window.location.port : '';
                    window.location.href = `http://${window.location.hostname}${port}/login`;
                } else if (error.status >= 500) { // catch all servererrors (statuscode >= 500)
                    this.toastr.error('Unbekannter Serverfehler! Bitte versuchen Sie es später erneut.');
                }

                return throwError(error);
            })
        );
    }

}
