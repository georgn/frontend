import {Injectable} from '@angular/core';

@Injectable()
export class CurrencyMaskService {
    private prefix: string;
    private decimalSeparator: string;
    private thousandsSeparator: string;

    constructor() {
        this.prefix = '';
        this.decimalSeparator = '.';
        this.thousandsSeparator = ',';
    }

    /**
     * Formats value for display use
     * @param value
     * @param allowNegative
     * @param decimalPrecision
     */
    transform(value: string, allowNegative = false, decimalPrecision: number = 2) {
        if (value == undefined || value === '') {
            return null;
        }

        if (value.toString().includes('.')) {
            var [integer, fraction = ''] = (value || '').toString().split('.');
        } else {
            var [integer, fraction = ''] = (value || '').toString().split(',');
        }

        // let [integer, fraction = ''] = (value || '').toString().split(this.decimalSeparator);
        fraction = decimalPrecision > 0 ? ',' + (fraction + '000000').substring(0, 2) : '';
        // integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, this.thousandsSeparator);
        return this.prefix + integer + fraction;
    }

    /**
     * Formats number for inner use
     * @param value
     * @param allowNegative
     */
    parse(value: string, allowNegative = false) {
        let [integer, fraction = ''] = (value || '').split(',');
        integer = integer.replace(new RegExp(/[^\d\.]/, 'g'), '');
        fraction = parseInt(fraction, 10) > 0 && 2 > 0 ? this.decimalSeparator + (fraction + '000000').substring(0, 2) : '';
        if (allowNegative && value.startsWith('-')) {
            return (-1 * parseFloat(integer + fraction)).toString();
        } else {
            return integer + fraction;
        }
    }
}
