import {Directive, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {JwtUtil} from '../util/jwt.util';
import {isUndefined} from 'util';

@Directive({
    selector: '[appCanAccess]'
})
export class CanAccessDirective implements OnInit, OnDestroy {
    @Input('appCanAccess') appCanAccess: string;

    constructor(private templateRef: TemplateRef<any>,
                private viewContainer: ViewContainerRef) {
    }

    ngOnInit(): void {
        this.applyPermission();
    }

    private applyPermission(): void {
        this.viewContainer.createEmbeddedView(this.templateRef);
        if (isUndefined(this.appCanAccess) || this.appCanAccess.length < 1) {
            return;
        }

        this.viewContainer.clear();

        const token = localStorage.getItem('token');
        if (!token) {
            return;
        }
        
    }

    ngOnDestroy(): void {

    }
}
