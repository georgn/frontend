export enum Role {
    // bpas
    BPA_EDIT = 'ROLE_BPA_EDIT',

    // article
    ARTICLE_EDIT = 'ROLE_ARTICLE_EDIT',
    ARTICLETYPE = 'ROLE_ARTICLETYPE',

    // supply
    SUPPLY = 'ROLE_SUPPLY',

    // reservation
    RESERVATION = 'ROLE_RESERVATION',

    // tyre24
    TYRE24 = 'ROLE_TYRE24',

    // accounting
    ACCOUNTING = 'ROLE_ACCOUNTING',
    SEPA = 'ROLE_SEPA',

    // stocktake
    STOCKTAKE = 'ROLE_STOCKTAKE',

    // gls
    GLS = 'ROLE_GLS',

    // stats
    STATISTIC = 'ROLE_STATISTIC',

    // user
    USER = 'ROLE_USER',

    // bucksheet
    BUCKSHEET = 'ROLE_BUCKSHEET',

    // settings
    SETTINGS = 'ROLE_SETTINGS'
}
