import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

@Injectable()
export class EnvService {
    private _articleLabelName: string;
    private _companyName: string;
    public enviroment = environment;

    get articleLabelName(): string {
        if (!this._articleLabelName) {
            this._articleLabelName = this.loadVar('article_number_label');
        }

        return this._articleLabelName;
    }

    get companyName(): string {
        if (!this._companyName) {
            this._companyName = this.loadVar('company_name');
        }
        return this._companyName;
    }

    private loadVar(varName: string): string {
        return localStorage.getItem('ENV_' + varName);
    }
}
