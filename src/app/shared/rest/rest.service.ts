import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

export class RestService<T> {
    private readonly url: string;

    constructor(private httpClient: HttpClient, endpoint: string, baseUrl?: string) {
        this.url = `${baseUrl || environment.url}/${endpoint}`;
    }

    // <-- BASIC HTTP METHODS
    public post(item: T, path?: string): Promise<T> {
        path = path || '';
        return new Promise<T>((resolve, reject) => {
            this.httpClient.post<T>(`${this.url}/${path}`, item).subscribe(
                data => {
                    resolve(data);
                },
                error => {
                    reject(error);
                }
            );
        });
    }

    public get(path?: string): Promise<T> {
        path = path || '';
        return new Promise<T>((resolve, reject) => {
            this.httpClient.get<T>(`${this.url}/${path}`).subscribe(
                data => {
                    resolve(data);
                },
                error => {
                    reject(error);
                }
            );
        });
    }


    public getList(path?: string): Promise<T[]> {
        path = path || '';

        return new Promise<T[]>((resolve, reject) => {
            this.httpClient.get<T[]>(`${this.url}/${path}`).subscribe(
                data => {
                    resolve(data);
                },
                error => {
                    reject(error);
                }
            );
        });
    }

    public getListGeneric<A>(path?: string): Promise<A[]> {
        path = path || '';

        return new Promise<A[]>((resolve, reject) => {
            this.httpClient.get<A[]>(`${this.url}/${path}`).subscribe(
                data => {
                    resolve(data);
                },
                error => {
                    reject(error);
                }
            );
        });
    }

    public delete(path?: string): Promise<any> {
        path = path || '';

        return new Promise<any>((resolve, reject) => {
            this.httpClient.delete(`${this.url}/${path}`).subscribe(
                data => {
                    resolve(data);
                }, error => {
                    reject(error);
                }
            );
        });
    }

    // BASIC HTTP METHOD -->
}
