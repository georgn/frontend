import {BaseClass} from "./base-class";

export class Employee extends BaseClass {
    name: string;
    firstname: string;
    mail: string;
    skills: Array<Skill> = [];
}

export class Skill {
    name: string;
    active: boolean;
}
