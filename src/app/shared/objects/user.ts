import {BaseClass} from "./base-class";

export class User extends BaseClass {
    static readonly PASSWORD_REGEX = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/g;

    public company: string;
    public password: string;
    public mail: string;
}
