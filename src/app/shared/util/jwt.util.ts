export class JwtUtil {
    public static decode(bearerToken: string): { exp, mail: string, userId, username: string } {
        if (!bearerToken || !bearerToken.includes('.')) {
            return null;
        }

        const base64Url = bearerToken.split('.')[1];
        const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');

        return JSON.parse(window.atob(base64));
    }
}
