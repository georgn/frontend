import {User} from '../objects/user';

export class Util {
    public static isUndefined(value: any): boolean {
        return typeof value === 'undefined';
    }

    public static isNullOrUndefined(value: any): boolean {
        return typeof value === 'undefined' || value === null;
    }

    public static objectIsEmpty(value: any): boolean {
        for (const key in value) {
            if (value.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }
}
