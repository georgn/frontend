import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {routerTransition} from '../router.animations';
import {AuthenticationService} from '../shared/services/authentication.service';
import {ToastrService} from "ngx-toastr";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    loading = false;
    mail: string = null;
    password: string = null;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private toastr: ToastrService
    ) {
    }

    ngOnInit() {
        this.authenticationService.logout();
    }

    login() {
        this.loading = true;
        const mail = this.mail;
        this.authenticationService.login(mail, this.password)
            .then(data => {
                localStorage.setItem('mail', mail);
                this.router.navigateByUrl('/');
            })
            .catch(error => {
                let errorMessage;
                switch (error.status) {
                    case 401:
                    case 403:
                        errorMessage
                            = 'Nutzername und Password stimmen nicht überein oder es existiert kein Nutzer mit diesen Namen!';
                        break;
                    case 0:
                        errorMessage = 'Der Server ist zurzeit nicht erreichbar!';
                        break;
                    case 500:
                    default:
                        errorMessage = 'Ein unbekannter Fehler ist aufgetreten!';
                }

                this.toastr.error(errorMessage)
            });
    }

}
