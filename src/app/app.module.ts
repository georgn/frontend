import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {registerLocaleData} from '@angular/common';
import localeDe from '@angular/common/locales/de';
import {AuthenticationService} from './shared/services/authentication.service';
import {StorageServiceModule} from 'angular-webstorage-service';
import {OWL_DATE_TIME_LOCALE} from 'ng-pick-datetime';
import {EnvService} from "./shared/config/env.service";
import {AuthGuard} from "./shared/guard";
import {UrlHistoryService} from "./shared/services/url-history.service";
import {ToastrModule} from "ngx-toastr";
import {JwtInterceptor} from "./shared/guard/JwtInterceptor";

registerLocaleData(localeDe);

// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};


@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        NgbModule,
        FormsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        StorageServiceModule,
        ToastrModule.forRoot(),
    ],
    declarations: [AppComponent],
    providers: [
        AuthGuard,
        AuthenticationService,
        {provide: OWL_DATE_TIME_LOCALE, useValue: 'de'},
        UrlHistoryService,
        EnvService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor() {

    }
}
